import Vue from 'vue';

import Example from '../components/Example.vue'
import App from '../components/App.vue'

Vue.component('App', App);

const app = new Vue({
    el: '#app',
    render: x => x(App),
    data: {
        name: 'Test'
    },
    components: {
        App, 
        Example
    },
});