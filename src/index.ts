import express from 'express';
import serveStatic from 'serve-static'
import dotenv from 'dotenv';
import PagesController from './controllers/PagesController'
dotenv.config();
const app = express();

app.use(serveStatic('dist/public'))

app.set('view engine', 'pug');

app.get('/', PagesController.index);

app.listen(process.env.PORT, () => {
    console.log(`App running. ( http://localhost:${process.env.PORT}/ )`);
});