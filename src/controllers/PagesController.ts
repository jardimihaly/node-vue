import * as express from 'express'

class PagesController
{
    public index(req: express.Request, res: express.Response)
    {
        res.render(
            'index'
        )
    }
}

export default new PagesController();