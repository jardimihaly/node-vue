import shelljs from 'shelljs';
import fs from 'fs';

if(!fs.existsSync('.env'))
{
    shelljs.cp('.env.example', '.env');
    console.log('.env file created. Please edit the variables to match your environment.');
}
